function se()

[filename, pathname] = uigetfile('*.txt','请选择一个数据txt文件');
if isequal(filename,0)
   return;
end

E = load(fullfile(pathname, filename)); % 加载网络

data = E(1:end, 2);
edges = E(1:end, 1);
n = length(data);

plot(1:n, data);
grid on
title('分支影响度曲线');
xlabel('分支编号');
ylabel('影响度');

% 定义界限
theta = 500;

% for i=1:n
%     if data(i) < theta
%         continue;
%     end
%     text(i, data(i), sprintf('e%d', edges(i)), 'HorizontalAlignment','center');
% end

end